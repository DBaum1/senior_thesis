# From Mainframes to Microchips: The Democratization of Computing

My senior thesis for UT Austin's [Undergraduate History Honors Program](https://liberalarts.utexas.edu/history/undergraduate/honors.php).

Divided into 3 core chapters, it surveys the various computing innovations (software and hardware) in the United States from World War II to the 1980s.